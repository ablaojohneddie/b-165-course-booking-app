//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalites [CREATE]
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNumber;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			gender: gendr,
			mobileNumber: mobil
		});

		return newUser.save().then((user, rejected) => {

			if (user) {
				return user; 
			} else {
				return 'Failed to Register';
			};
		});
	};

//[SECTION] Functionalites [RETRIEVE]
//[SECTION] Functionalites [UPDATE]
//[SECTION] Functionalites [DELETE]
